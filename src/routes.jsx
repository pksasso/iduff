import React from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';

import Login from './pages/Login/Login';
import Curriculo from './pages/Curriculo/Curriculo';
import Historico from './pages/Historico/Historico';
import NotFound from './pages/NotFound/NotFound';
import EditProfile from './pages/EditProfile/EditProfile';
import Inscricao from './pages/Inscricao/Inscricao';
import Turmas from './pages/Turmas/Turmas';
import CadastroTurmas from './pages/Turmas/CadastroTurmas/CadastroTurmas';
import ListaTurmas from './pages/Turmas/ListaTurmas/ListaTurmas';
import TurmasProfessor from './pages/Turmas/TurmaProfessor/TurmasProfessor';
import TurmasAluno from './pages/Turmas/TurmasAluno/TurmasAluno';
import EdicaoDepartamentoDi from './pages/Curso/EdicaoDepartamentoDi/EdicaoDepaertamentoDi';
import CoordenadorCad from './pages/Curso/Cadastro-coordenador/Cadastro-coordenador';
import CursoDiretor from './pages/Curso/CursoDiretor/CursoDiretor';
import ProfessorCad from './pages/Curso/Cadastro-professor/Cadastro-professor';
import AlunoCad from './pages/Curso/Cadastro-aluno/Cadastro-aluno';
import CursoCoordenador from './pages/Curso/CursoCoordenador/CursoCoordenador';
import EdicaoDepartamentoCod from './pages/Curso/EdicaoDepartamentoCod/EdicaoDepartamentoCod';
import SchoolYear from './pages/SchoolYear/SchoolYear';
import AddMaterias from './pages/AdicionarMaterias/AddMaterias';
import Professor from './pages/Professor/Professor';
import ListasAlunos from './pages/Listas/ListasAluno/ListasAluno';
import ListasCoordenador from './pages/Listas/ListasCoordenador/ListasCoordenador';
import ListasProfessor from './pages/Listas/ListasProfessor/ListasProfessor';

function CustomRoute({ isPrivate, ...rest }) {
  if (isPrivate && !localStorage.getItem('token')) {
    return <Redirect to='/login' />;
  }
  return <Route {...rest} />;
}

function Routes() {
  const loggedData = JSON.parse(localStorage.getItem('userData'));

  return (
    <Router>
      <Switch>
        <Route exact path='/'>
          <Redirect to={'/login'} />
        </Route>
        <Route exact path='/login' component={Login} />

        <CustomRoute isPrivate exact path='/curriculo'>
          <Curriculo />
        </CustomRoute>

        <CustomRoute isPrivate exact path='/editarperfil' component={EditProfile} />

        <CustomRoute isPrivate exact path='/inscricao' component={Inscricao} />

        <CustomRoute isPrivate exact path='/cadastro-aluno' component={AlunoCad} />
        
        <CustomRoute isPrivate exact path='/cadastro-coordenador' component={CoordenadorCad} />
        
        <CustomRoute isPrivate exact path='/editar-curso' component={CursoDiretor} />
        
        <CustomRoute isPrivate exact path='/cadastro-professor' component={ProfessorCad} />

        <CustomRoute isPrivate exact path='/historico' component={Historico} />

        <CustomRoute isPrivate exact path='/turmas' component={Turmas} />

        <CustomRoute isPrivate exact path='/listaturmas' component={ListaTurmas} />

        <CustomRoute isPrivate exact path='/cadastroturmas' component={CadastroTurmas} />

        <CustomRoute isPrivate exact path='/turmas' component={Turmas} />

        <CustomRoute isPrivate exact path='/cadastroturmas' component={CadastroTurmas} />

        <CustomRoute isPrivate exact path='/turmasprofessor' component={TurmasProfessor} />
        
        <CustomRoute isPrivate exact path='/turmasaluno' component={TurmasAluno} />

        <CustomRoute isPrivate exact path='/ano-letivo' component={SchoolYear} />
        
        <CustomRoute isPrivate exact path='/cursocoordenador' component={CursoCoordenador} />

        <CustomRoute isPrivate exact path='/addmaterias' component={AddMaterias} />

        <CustomRoute isPrivate exact path='/professor' component={Professor} />
        
        <CustomRoute isPrivate exact path='/listas-alunos' component={ListasAlunos} />
        
        <CustomRoute isPrivate exact path='/listas-coordenador' component={ListasCoordenador} />
        
        <CustomRoute isPrivate exact path='/listas-professor' component={ListasProfessor} />
        
        <CustomRoute isPrivate exact path='/edicao-departamento' component={EdicaoDepartamentoCod} />
        
        <CustomRoute isPrivate exact path='/departamento-edicao' component={EdicaoDepartamentoDi} />

        <Route
          path='/'
          component={
            {
              /*componente de 404*/
            }
          }
        />
      </Switch>
    </Router>
  );
}

export default Routes;
