import Router from './routes';
import {Provider} from './context/Context';
import './root.css';
import './App.css';

function App() {
  return (
    <div className='App'>
      <Provider>
        <Router />
      </Provider>
    </div>
  );
}

export default App;
