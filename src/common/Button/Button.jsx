import React from 'react';
import { useHistory } from 'react-router-dom';
import './Button.css';

function Button({ text, nextPath, handleClick, buttonLocation }) {
  const history = useHistory();

  const selectClick = () => {
    if (handleClick) {
      handleClick();
    }
    if (nextPath) {
      history.push(nextPath);
    }
  };

  return (
    <button
      className={`button ${buttonLocation}`}
      onClick={() => {
        selectClick();
      }}
    >
      {text}
    </button>
  );
}

export default Button;
