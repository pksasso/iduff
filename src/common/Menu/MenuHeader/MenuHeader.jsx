import React from 'react';
import { useLocation } from 'react-router-dom';
import Button from '../../Button/Button';
import './MenuHeader.css';

function MenuHeader({ editData }) {
  const location = useLocation();

  const loggedData = JSON.parse(localStorage.getItem('userData'));

  const formatCoursename = () => {
    if (loggedData.role === 'manager') {
      return 'Diretor';
    }
    if (loggedData.role === 'professor') {
      return 'Professor';
    }
    if (loggedData.role === 'coordinator') {
      return `Coordenador de ${loggedData.informations_role.name}`;
    }
  };

  return (
    <div className='menu-header'>
      <p>{loggedData.name}</p>
      <p>
        {loggedData.role === 'student'
          ? loggedData.informations_role.type_coordinator
          : formatCoursename(loggedData)}
      </p>
      <p>M. {loggedData.informations_role.registration}</p>
      {location.pathname !== '/editarperfil' ? (
        <Button
          text='Editar dados'
          nextPath='/editarperfil'
          buttonLocation='button-menu-header'
          handleClick={() => editData()}
        />
      ) : (
        ''
      )}
    </div>
  );
}

export default MenuHeader;
