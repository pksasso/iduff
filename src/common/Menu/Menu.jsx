import React, { useContext } from 'react';
import MenuHeader from './MenuHeader/MenuHeader';
import Button from '../Button/Button';
import './Menu.css';
import { Context } from '../../context/Context';
import { useHistory } from 'react-router-dom';

function Menu() {
  const { handleLogout } = useContext(Context);
  const history = useHistory();

  const loggedData = JSON.parse(localStorage.getItem('userData'));

  const managerButtons = () => {
    return (
      <>
        <Button 
          className = 'botaomenu'
          text='Departamento'
          buttonLocation='menu-button'
          handleClick={() => history.push('/departamento-edicao')}
        />
        <Button 
          className = 'botaomenu'
          text='Cursos'
          buttonLocation='menu-button'
          handleClick={() => history.push('/editar-curso')}
        />
        <Button 
          className = 'botaomenu'
          text='Coordenadores'
          buttonLocation='menu-button'
          handleClick={() =>  history.push('/listas-coordenador')}
        />
        <Button 
          className = 'botaomenu'
          text='Curriculos'
          buttonLocation='menu-button'
          handleClick={() => history.push('/curriculo')}
        />
      </>
    );
  };

  const headDepartmentButtons = () => {
    return (
      <>
        <Button 
          className = 'botaomenu'
          text='Departamento'
          buttonLocation='menu-button'
          handleClick={() => history.push('/edicao-departamento')}
        />
        <Button 
          className = 'botaomenu'
          text='Curriculos'
          buttonLocation='menu-button'
          handleClick={() => history.push('/curriculo')}
        />
        <Button 
          className = 'botaomenu'
          text='2020.2'
          buttonLocation='menu-button'
          handleClick={() => console.log('2020.2')}
        />
        <Button 
          className = 'botaomenu'
          text='Professores'
          buttonLocation='menu-button'
          handleClick={() => history.push('/listas-professor')}
        />
        <Button 
          className = 'botaomenu'
          text='Turmas'
          buttonLocation='menu-button'
          handleClick={() => history.push('/listaturmas')}
        />
      </>
    );
  };

  const courseHeadButtons = () => {
    return (
      <>
        <Button 
          className = 'botaomenu'
          text='Curso'
          buttonLocation='menu-button'
          handleClick={() => history.push('/cursocoordenador')}
        />
        <Button 
          className = 'botaomenu'
          text='Alunos'
          buttonLocation='menu-button'
          handleClick={() => history.push('/listas-alunos')}
        />
        <Button 
          className = 'botaomenu'
          text='Curriculo'
          buttonLocation='menu-button'
          handleClick={() => history.push('/curriculo')}
        />
      </>
    );
  };

  const professorButtons = () => {
    return (
      <>
        <Button 
          className = 'botaomenu'
          text='Turmas'
          buttonLocation='menu-button'
          handleClick={() => history.push('/turmas')}
        />
        <Button 
          className = 'botaomenu'
          text='Curriculo'
          buttonLocation='menu-button'
          handleClick={() => history.push('/curriculo')}
        />
      </>
    );
  };

  const studentButtons = () => {
    return (
      <>
        <Button 
          className = 'botaomenu'
          text='Histórico'
          buttonLocation='menu-button'
          handleClick={() => history.push('/historico')}
        />
        <Button 
          className = 'botaomenu'
          text='Curriculo'
          buttonLocation='menu-button'
          handleClick={() => history.push('/curriculo')}
        />
        <Button 
          className = 'botaomenu'
          text='Turmas'
          buttonLocation='menu-button'
          handleClick={() => history.push('/turmasaluno')}
        />
        <Button 
          className = 'botaomenu'
          text='Inscrição online'
          buttonLocation='menu-button'
          handleClick={() => history.push('/inscricao')}
        />
      </>
    );
  };

  const selectButtons = (user) => {
    if (user.role === 'student') {
      return studentButtons();
    }
    if (user.role === 'professor') {
      return professorButtons();
    }
    if (user.role === 'coordinator' && user.informations_role.type_coordinator === 'course') {
      return courseHeadButtons();
    }
    if (user.role === 'coordinator' && user.informations_role.type_coordinator === 'department') {
      return headDepartmentButtons();
    }
    if (user.role === 'manager') {
      return managerButtons();
    }
  };

  return (
    <div className='menu-wrapper'>
      <MenuHeader user={loggedData} editData={() => history.push('/editarperfil')} />
      <div className='menu-list-button'>
        {selectButtons(loggedData)}
        <Button 
          className = 'botaomenu'
          text='Sair'
          buttonLocation='menu-button'
          handleClick={() => handleLogout(history)}
        />
      </div>
    </div>
  );
}

export default Menu;
