import React from 'react';
import './Input.css';

function Input({ labelText, state, setState, labelPosition, inputType }) {
  return (
    <div className={`input-wrapper ${labelPosition}`}>
      <label className='input-label'>{labelText}</label>
      <input
        className='input-insert'
        type={inputType}
        value={state}
        onChange={(e) => setState(e.target.value)}
      />
    </div>
  );
}

export default Input;
