import React from 'react';
import './Table.css';
import Button from '../Button/Button';

import { useLocation } from 'react-router-dom';

function Table({ titles, items }) {
  const location = useLocation();

  const loggedData = JSON.parse(localStorage.getItem('userData'));

  const generateTitle = () => {
    return (
      <tr>
        {titles.map((title) => {
          return <td>{title}</td>;
        })}
      </tr>
    );
  };

  const selectRows = (item) => {
    if (location.pathname === '/curriculo') {
      return (
        <>
          <td key={`${item.name}-${item.id}`}>{item.name}</td>
          {loggedData.role === 'manager' || loggedData.role === 'coodinator' ? (
            <td>
              <select name='Periodo' className='select-period-curriculum'>
                <option value='1º Periodo'>1º Periodo</option>
                <option value='2º Periodo'>2º Periodo</option>
                <option value='3º Periodo'>3º Periodo</option>
                <option value='4º Periodo'>4º Periodo</option>
              </select>
            </td>
          ) : (
            <td key={`${item.semester}-${item.id}`}>{`${item.semester}° Semestre`}</td>
          )}
          <td key={`${item.workload}-${item.id}`}>{item.workload}</td>
        </>
      );
    }
    if (location.pathname === '/inscricao') {
      return (
        <>
          <td>{item.name}</td>
          <td>{item.vagas}</td>
          <td>{item.period}</td>
          <td>{item.cht}</td>
        </>
      );
    }
    if (location.pathname === '/historico') {
      return (
        <>
          <td>{item.name}</td>
          <td>{item.status}</td>
          <td>{item.nota}</td>
        </>
      );
    }
    if (location.pathname === '/turmas') {
      return (
        <>
          <td>{item.name}</td>
          <td>{item.code}</td>
          <td>{item.time}</td>
          <td>{item.room}</td>
        </>
      );
    }
    if (location.pathname === '/periodoletivo') {
      return (
        <>
          <td>{item.name}</td>
          <td>{item.knowledge_area}</td>
          <td>{item.quantity}</td>
        </>
      );
    }
  };

  const generateLines = () => {
    return items.map((item) => {
      return (
        <tr>
          {selectRows(item)}
          {loggedData.role === 'student' && location.pathname === '/inscricao' ? (
            <td>
              <Button
                text='Inscrever'
                buttonLocation='prof-curriculum'
                handleClick={() => console.log(item.id)}
              />
            </td>
          ) : null}
          {loggedData.role === 'professor' && location.pathname === '/turmas' ? (
            <td key={item.id}>
              <Button
                text='Gerenciar'
                buttonLocation='prof-curriculum'
                handleClick={() => console.log(item.id)}
              />
            </td>
          ) : null}
        </tr>
      );
    });
  };

  return (
    <div className='test'>
      <div className='table-container'>
        <table className='table-wrapper'>
          <thead>{generateTitle()}</thead>
          <tbody>{generateLines()}</tbody>
        </table>
      </div>
    </div>
  );
}

export default Table;
