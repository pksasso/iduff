import axios from 'axios';

const api = axios.create({
  // baseURL: 'https://quiet-peak-28566.herokuapp.com',
  baseURL: 'https://rocky-savannah-33998.herokuapp.com',
});

export { api };