import React, { createContext, useState, useEffect } from 'react';

import { api } from '../service/api';

const Context = createContext();

const Provider = ({ children }) => {
  const [isAuth, setIsAuth] = useState(false);
  const [authLoading, setAuthLoading] = useState(true);
  const [allCourses, setAllCourses] = useState([]);
  const [allDepartments, setAllDepartments] = useState([]);
  const [allSubjectsByDepartment, setAllSubjectsByDepartment] = useState([]);
  const [loggedData, setLoggedData] = useState({});

  useEffect(() => {
    const token = localStorage.getItem('token');
    const userData = localStorage.getItem('userData');
    if (token) {
      api.defaults.headers.Authorization = token;
      setLoggedData(JSON.parse(userData));
      setIsAuth(true);
    }
    setAuthLoading(false);
  }, []);

  const handleLogin = (cpf, password, history) => {
    api
      .post('/login', {
        user: {
          cpf,
          password,
        },
      })
      .then((res) => {
        console.log(res);
        let token = res.data.token;
        localStorage.setItem('token', token);
        api.defaults.headers.Authorization = token;
        setIsAuth(true);
      })
      .then(() => {
        getPersonalInfo(history);
      });
  };

  const getPersonalInfo = (history) => {
    const config = {
      headers: {
        Authorization: localStorage.getItem('token'),
      },
    };
    api
      .get('/users/1', config)
      .then((res) => {
        localStorage.setItem('userData', JSON.stringify(res.data));
      })
      .then(() => history.push('/curriculo'));
  };

  const handleLogout = (history) => {
    localStorage.removeItem('token');
    localStorage.removeItem('userData');
    api.defaults.headers.Authorization = undefined;
    setIsAuth(false);
    setLoggedData({});
    history.push('/login');
  };

  const getAllDepartments = () => {
    api.get('/departments').then((res) => {
      setAllDepartments(res.data);
    });
  };

  const getAllSubjectFromDepartments = (idDepartment) => {
    api.get('/subjects').then((res) => {
      let a = res.data.map((subject) => {
        if (subject.department === idDepartment) {
          return subject;
        }
      });
      console.log(a);
      setAllSubjectsByDepartment(res.data);
    });
  };

  const pegarAlunos = 'ola';

  return (
    <Context.Provider
      value={{
        allDepartments,
        getAllSubjectFromDepartments,
        handleLogin,
        isAuth,
        setIsAuth,
        authLoading,
        handleLogout,
        allSubjectsByDepartment,
        loggedData,
        getAllDepartments,
        allCourses,
      }}
    >
      {children}
    </Context.Provider>
  );
};

export { Context, Provider };
