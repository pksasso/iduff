import React, { useState, useContext } from 'react';
import './Login.css';
import Button from '../../common/Button/Button';
import Input from '../../common/Input/Input';
import { Context } from '../../context/Context';
import { useHistory } from 'react-router-dom';

function Login() {
  const [login, setLogin] = useState('124.525.115-00');
  const { pegarAlunos} = useContext(Context);
  const [password, setPassword] = useState('123456');
  const { handleLogin} = useContext(Context);
  console.log(pegarAlunos);
  
  const history = useHistory();

  const postLogin = () => {
    handleLogin(login, password, history);
  };

  const cpfMask = (value) => {
    const cpf = value
      .replace(/\D/g, '')
      .replace(/(\d{3})(\d)/, '$1.$2')
      .replace(/(\d{3})(\d)/, '$1.$2')
      .replace(/(\d{3})(\d{1,2})/, '$1-$2')
      .replace(/(-\d{2})\d+?$/, '$1');
    setLogin(cpf);
  };

  return (
    <div className='login-wrapper'>
      <div className='square'>
        <div className='square2'>
          <h1 className='fonte1'>IdUFF</h1>
          <h2 className='fonte2'>Bem vindo ao Sistema Acadêmico da Graduação</h2>
          <h3 className='fonte3'>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris mattis ex vel mauris
            porta pulvinar. Integer luctus bibendum ex et faucibus. Vivamus sagittis consectetur
            quam et rhoncus. Cras imperdiet nisi et elementum dictum. Duis nec turpis eleifend,
            viverra turpis et, egestas felis. Orci varius natoque penatibus et magnis dis parturient
            montes, nascetur ridiculus mus. Phasellus quis posuere sem.
          </h3>
        </div>
        <div className='square3'>
          <h3 className='fonte4'>Acesse o seu idUFF</h3>
          <div className='form1'>
            <div className='login-form-1'>
              <Input
                labelText='Seu CPF (somente números)'
                state={login}
                setState={cpfMask}
                labelPosition='label-top'
              />
            </div>
            <Input
              labelText='Senha do seu idUFF:'
              inputType='password'
              state={password}
              setState={setPassword}
              labelPosition='label-top'
            />
            <div className='login-button-wrapper'>
              <Button text='Logar' buttonLocation='login-button' handleClick={() => postLogin()} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Login;