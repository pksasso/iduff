import React from 'react';
import './CadastroTurmas.css';
import Menu from '../../../common/Menu/Menu';
import { useHistory } from 'react-router-dom';

function CadastroTurmas() {
  const history = useHistory();
  return (
    // BASE
    <div className='base-screen'>
      <Menu />
      <div className='base-content'>
        {/* Começo do site */}
        <button
          onclick='myFunction()'
          className='boxexit'
          onClick={() => history.push('/listaturmas')}>X</button>{' '}
        <h1 id='titulo'>Turmas</h1>
        <br />
        <div className='linhatitulo'></div>
        <div className='caixatexto'>
          {/*TABELA NOME*/}
          <h3 className='TableSub'>Departamento: GMA</h3>
          <br />
          <form action='' className='row'>
            <p>
              <label for='' className='TableSubs'>
                Nome:
              </label>
              <input type='text' className='tamanhos' />
            </p>
            <p>
              <label for='' className='TableSubs'>
                Sala:
              </label>
              <input type='text' className='tamanhos' />
            </p>
          </form>
          <br />
          <form action=''>
            <p class='formmeio'>
              <label for='' className='TableSub'>
                Calendário
              </label>
              &nbsp;&nbsp;&nbsp;
              <input type='text' className='tamanhos2' />
            </p>
            <table>
              <tbody>
                {' '}
                <br />
                <tr>
                  <td className='TableSubs'>Matéria:</td>
                  <td>
                    <select name='exemplo1' className='tamanho' />
                  </td>
                </tr>
                <br />
                <tr>
                  <td className='TableSubs'>Professor:</td>
                  <td>
                    <select name='exemplo1' className='tamanho' />
                  </td>
                </tr>
              </tbody>
            </table>
            <br />
            <br />
            <p class='formmeio'>
              <button onclick='myFunction()' class='buttoncarac2'>
                Salvar
              </button>{' '}
            </p>
          </form>
        </div>
      </div>
    </div>
  );
}
export default CadastroTurmas;
