import React from 'react';
import './TurmasProfessor.css';
import Menu from '../../../common/Menu/Menu';
import { useHistory } from 'react-router-dom';

function TurmasProfessor() {
  const history = useHistory();
  const loggedData = JSON.parse(localStorage.getItem('userData'));
  //API---------------------------
  //
  //
  //
  //API---------------------------

  const teste = [
    {
      name: 'Juliana',
      grade1: '4',
      grade2: '8',
    },
    {
      name: 'Murilo',
      grade1: '1',
      grade2: '2',
    },
    {
      name: 'Sara',
      grade1: '6',
      grade2: '7',
    },
    {
      name: 'Chris',
      grade1: '9',
      grade2: '7',
    },
    {
      name: 'Lola',
      grade1: '6',
      grade2: '4',
    },
    {
      name: 'Bruno',
      grade1: '5',
      grade2: '3',
    },
  ];

  return (
    <div className='base-screen'>
      <Menu />
      <div className='base-content'>
        {/* CAIXA */}
        <div id='caixaTP'>
          {/* TEXTO */}
          <button
          onclick='myFunction()'
          className='boxexit'
          onClick={() => history.push('/turmas')}>X</button>{' '}
          <h1 id='titulo'>Turmas</h1><br/><br/>
          <div className = 'linhatitulo'></div>
          <div className='invisivelTP'>
            <h2>Cálculo I</h2>
            <h2>A1</h2>
            <div className='course-student-name'>
            {loggedData.role === 'professor' ? 'Professor: ' : 'Aluno : '}
            {loggedData.name}
            </div>
            <h5 className='Subs'>Calendario: Seg e Qua, 14h as 16h</h5>
            <br />
            {/* NOME PRA TABELA */}

            <div className='tamanhoTP'>
              <th className='Subs'>Lançamentos de notas</th>
            </div>
          </div>
          {/* TABELA */}
          {/* API AQUI ----------------------------------------------- */}
          {teste.map((u) => (
            <tr key={u.key} className='caixafinalTP1'>
              <td>
                <div>{u.name}</div>
              </td>
              <div className='caixafinalTP2'>
                <td>{u.grade1}</td>
                <td>{u.grade2}</td>
                <div><button
                          onclick='myFunction()'
                          className="boxmanager"
                          onClick={() => history.push('/turmasprofessor')}>Enviar</button>{' '}
                </div>
              </div>
            </tr>
            //API!!!!!!//
          ))}
        </div>
      </div>
    </div>
  );
}

export default TurmasProfessor;
