import React from 'react';
import './Turmas.css';
import Menu from '../../common/Menu/Menu';
import { useHistory } from 'react-router-dom';

function Turmas() {
  const history = useHistory();
  const loggedData = JSON.parse(localStorage.getItem('userData'));
  //API---------------------------
  //
  //
  //
  //API---------------------------

  const teste = [
    {
      name: 'Calculo I',
      code: 'A1',
      time: 'Seg e Qua, 14h as 16h',
      room: '401',
    },
    {
      name: 'Calculo II',
      code: 'A1',
      time: 'Ter e Qui, 14h as 16h',
      room: '402',
    },
    {
      name: 'Calculo I',
      code: 'A1',
      time: 'Seg e Qua, 14h as 16h',
      room: '401',
    },
    {
      name: 'Calculo II',
      code: 'A1',
      time: 'Ter e Qui, 14h as 16h',
      room: '402',
    },
    {
      name: 'Calculo I',
      code: 'A1',
      time: 'Seg e Qua, 14h as 16h',
      room: '401',
    },
    {
      name: 'Calculo II',
      code: 'A1',
      time: 'Ter e Qui, 14h as 16h',
      room: '402',
    },
  ];
  
  return (
    
    <div className='base-screen'>
      <Menu />
      <div className='base-content'>
        <div>
          <h1 id='titulo'>Turmas</h1><br/><br/>
          <div className = 'linhatitulo'></div>
          <div className='course-student-name'>
            {loggedData.role === 'professor' ? 'Professor: ' : 'Aluno : '}
            {loggedData.name}
          </div>
          <div className = 'subflor'>
            <div>Nome</div>
            <div>Código</div>
            <div>Calendário</div>
            <div>Sala</div>
          </div>
          {/* API AQUI ----------------------------------------------- */}
            <div className="meio">
              {teste.map(u => (
                <div className = 'tabelaflor'>
                  <div>{u.name}</div>
                  <div>{u.code}</div>
                  <div>{u.time}</div>
                  <div>{u.room}</div>
                  <div><button
                          onclick='myFunction()'
                          className="boxmanager"
                          onClick={() => history.push('/turmasprofessor')}>Gerenciar</button>{' '}
                  </div>
                </div>
                
              ))}
          </div>
      {/* //API!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */}
        </div>
      </div>
    </div>
  );
}

export default Turmas;
