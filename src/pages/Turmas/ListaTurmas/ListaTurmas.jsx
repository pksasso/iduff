import React from 'react';
import './ListaTurmas.css';
import Menu from '../../../common/Menu/Menu';
import { useHistory } from 'react-router-dom';

function ListaTurmas({ title }) {
  const history = useHistory();
  return (
    // BASE
    <div className='base-screen'>
      <Menu />
      <div className='base-content'>
        {/* Começo do site */}
        <button type='submit' className='boxadd' onClick={() => history.push('/cadastroturmas')}>
          +
        </button>
        <h1 id='titulo'>Turmas</h1>
        <br />
        <div className='linhatitulo'></div>
        <div className='caixatexto'>
          {/*TABELA NOME*/}
          <h3 className='TableSub'>Ano letivo: 2020.2</h3>
          <h3 className='TableSub'>Status: Planejamento</h3>
          <br />
        </div>
      </div>
    </div>
  );
}
export default ListaTurmas