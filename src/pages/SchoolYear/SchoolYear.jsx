import React, { useContext } from 'react';
import Button from '../../common/Button/Button';
import Menu from '../../common/Menu/Menu';
import Table from '../../common/Table/Table';
import { Context } from '../../context/Context';
import './SchoolYear.css';
import { useHistory } from 'react-router-dom';

const teste = [
  {
    id: 1,
    name: 'Calculo I',
    knowledge_area: 'Matematica',
    quantity: '60',
  },
  {
    id: 2,
    name: 'Prog II',
    knowledge_area: 'Prog I',
    quantity: '60',
  },
  {
    id: 3,
    name: 'PROBEST',
    knowledge_area: 'Calculo',
    quantity: '70',
  },
  {
    id: 4,
    name: 'FMC',
    knowledge_area: 'Matematica',
    quantity: '60',
  },
  {
    id: 5,
    name: 'SO',
    knowledge_area: 'FAC',
    quantity: '60',
  },
  {
    id: 6,
    name: 'Empreendedorismo',
    knowledge_area: 'Adm',
    quantity: '45',
  },
  {
    id: 7,
    name: 'Calculo II',
    knowledge_area: 'Calculo I',
    quantity: '80',
  },
];

function SchoolYear() {
  const { loggedData } = useContext(Context);
  const history = useHistory();
   return (
      <div className='base-screen'>
        <Menu />
        <div className='base-content'>
          <h1 id='titulo'>Periodo Letivo</h1>
          <div className='schoolyear-head'>
            <div className='schoolyear-data'>
              <p>Ano letivo atual: 2020.2</p>
              <p>Status: Concluido</p>
            </div>
            <div className='schoolyear-planner'>
              <Button text='Planejar 2021.1' buttonLocation='edit-button-wrapper' />
            </div>
          </div>
          <div className='schoolyear-head'>
            <div className='schoolyear-add-subject'>
              <p>Matérias oferecidas pelo departamento: </p>
            </div>
            <div className='schoolyear-planner'>
              <Button text='Adicionar Matéria' buttonLocation='edit-button-wrapper' />
            </div>
          </div>
          <div className = 'flor2'>
            <div>Nome</div>
            <div>Area de Conhecimento</div>
            <div>Total de vagas</div>
          </div>
          {/* API AQUI ----------------------------------------------- */}
          <div className="Cardsprof">
            {teste.map(u => (
              <div className = 'flor'>
                <div>{u.name}</div>
                <div>{u.knowledge_area}</div>
                <div>{u.quantity}</div>
                <div><button
                        onclick='myFunction()'
                        id="deleteprof"
                        onClick={() => history.push('/ano-letivo')}>Editar</button>{' '}
                </div>
              </div>
              
            ))}
        </div>
          {/* //API!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */}
        </div>
      </div>  
  );
}

export default SchoolYear;
