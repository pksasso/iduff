import React, { useEffect, useState, useContext } from 'react';
import './Curriculo.css';
import Menu from '../../common/Menu/Menu';
import { Context } from '../../context/Context';
import Table from '../../common/Table/Table';

function Curriculo() {
  const {
    allSubjectsByDepartment,
    getAllSubjectFromDepartments,
    getAllDepartments,
    allDepartments,
  } = useContext(Context);
  const [subjectsList, setSubjectsList] = useState([]);
  const [selectedDropDown, setSelectedDropDown] = useState(1);

  useEffect(() => {
    getAllDepartments();
    getSubjects();
  }, []);

  const getSubjects = async () => {
    await getAllSubjectFromDepartments(selectedDropDown);
  };

  const teste = [
    {
      id: 1,
      name: 'Calculo I',
      period: '2° periodo',
      cht: '60',
    },
    {
      id: 2,
      name: 'Prog I',
      period: '1° periodo',
      cht: '60',
    },
    {
      id: 3,
      name: 'PROBEST',
      period: '3° periodo',
      cht: '70',
    },
    {
      id: 4,
      name: 'FMC',
      period: '1° periodo',
      cht: '60',
    },
    {
      id: 5,
      name: 'SO',
      period: '3° periodo',
      cht: '60',
    },
    {
      id: 6,
      name: 'Empreendedorismo',
      period: '5° periodo',
      cht: '45',
    },
    {
      id: 7,
      name: 'Calculo II',
      period: '7° periodo',
      cht: '80',
    },
    {
      id: 8,
      name: 'Prog II',
      period: '9° periodo',
      cht: '100',
    },
    {
      id: 9,
      name: 'Empreendedorismo',
      period: '5° periodo',
      cht: '45',
    },
    {
      id: 10,
      name: 'Calculo II',
      period: '7° periodo',
      cht: '80',
    },
    {
      id: 11,
      name: 'Prog II',
      period: '9° periodo',
      cht: '100',
    },
  ];

  const generateCourseOptions = () => {
    const courseGroup = (
      <>
        <option value=''>Selecione</option>;
        {allDepartments.map((department) => {
          return (
            <option
              name={department.name}
              onClick={() => {
                console.log(department.id);
                // getSubjects(department.id);
              }}
              value={department.id}
            >
              {department.name}
            </option>
          );
        })}
      </>
    );
    return courseGroup;
  };

  return (
    <div className='base-screen'>
      <Menu />
      <div className='base-content'>
        <h1 className='title-wrapper' id='titulo'>
          Visualizar Currículos
        </h1>
        <div className='curriculum-course-title-select'>
          <p>Curso:</p>
          <select name='exemplo1' className='seleciona' onChange={() => console.log('torquei')}>
            {allDepartments ? generateCourseOptions() : <option value=''>Carregando...</option>}
          </select>
        </div>
        <Table titles={['Nome', 'Período', 'CHT']} items={allSubjectsByDepartment} />
      </div>
    </div>
  );
}
export default Curriculo;
