import React from 'react';
import Menu from '../../../common/Menu/Menu';
import { useLocation} from 'react-router-dom';
import { useHistory } from 'react-router-dom';

function EdicaoDepartamentoCod() {
    const history = useHistory();
    const location = useLocation();
    const loggedData = JSON.parse(localStorage.getItem('userData'));
  
    const title = location.pathname.split('/')[2];
  
    return (
      // BASE
      <div className='base-screen'>
        <Menu />
        <div className='base-content'>
            {/* Começo do site */}
            <h1 id='titulo'>Curso</h1><br/><br/>
            <div className='linhatitulo'></div><br/><br/>
                <h3 className='TableSub'>Edite as informações do Curso</h3>
                <div> 
                    <div className='wordsize'>
                        Nome do Curso:
                        <input type='text' className='barratamanho' />
                    </div>
                    <div className='wordsize'>
                        Área de Conhecimente:
                        <input type='text' className='barratamanho' />
                    </div>
                    <div className='wordsize'>
                        Campus/Sede:
                        <input type='text' className='barratamanho' />
                    </div>
                    <div className='wordsize'>
                        Coordenador:
                        <select name='exemplo1' className='barratamanho' />
                    </div><br/><br/>
                    <form action='' className='row'>
                        <p>
                            <label for='' className='TableSubs'>
                            Telefone:
                            </label>
                            <input type='text' className='tamanhos' />
                        </p>
                        <p>
                            <label for='' className='TableSubs'>
                            Email:
                            </label>
                            <input type='text' className='tamanhos' />
                        </p>
                    </form>
                    <p><br/><br/>
                    <button onclick='myFunction()' class='buttoncara'>
                        Salvar
                    </button>{' '}
                    </p>
                </div>
          </div>
    </div>
  );
}
export default EdicaoDepartamentoCod;