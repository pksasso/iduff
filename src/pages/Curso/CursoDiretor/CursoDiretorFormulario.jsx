import React from 'react';
import './CursoDiretorFormulario.css';
import Menu from '../../../common/Menu/Menu';
import Input from '../../../common/Input/Input';
import Button from '../../../common/Button/Button';
import { Context } from '../../../context/Context';

function CursoDiretor() {
  return (
    // BASE
    <div className='base-screen'>
      <Menu />
      <div className='base-content'>
        {/* Começo do site */}
        <h1 id='titulo'>Curso</h1>
        <div className='caixatexto'>
          <h3 className='TableSub'>Edite as informações do curso</h3>
          {/*TABELA NOME*/}
          <div>
            <form>
              <table>
                <tbody>
                  <tr>
                    <td className='TableSub'>Nome do Curso:</td>
                    <td>
                      <input type='text' className='textbox' />
                    </td>
                  </tr>
                  <tr>
                    <td className='TableSub'>Área de conhecimento:</td>
                    <td>
                      <input type='text' className='textbox' />
                    </td>
                  </tr>
                  <tr>
                    <td className='TableSub'>Campus/Sede:</td>
                    <td>
                      <input type='text' className='textbox' />
                    </td>
                  </tr>
                  <tr>
                    <td className='TableSub'>Coordenador:</td>
                    <td>
                      <select name='exemplo1' className='textbox' />
                    </td>
                  </tr>
                </tbody>
              </table>
            </form>
          </div>
        </div>
        <div className='caixatexto'>
          <h3 className='TableSub2'>Dados para contato</h3> <br />
          <div className='TableRow'>
            <tr>
              <td className='TableSub2'>Telefone</td>
              <br />
              <td>
                <input
                  type='text'
                  class='form-control phone-ddd-mask'
                  className='textbox2'
                  placeholder='Ex.: (00) 0000-0000'
                />
              </td>
            </tr>
            <div className='space'></div>
            <tr>
              <td className='TableSub2'>Email</td>
              <br />
              <td>
                <input type='email' className='textbox3' />
              </td>
            </tr>
          </div>
        </div>
        <br />
        <br />
        <div className='helpbot'>
          {' '}
          <button onclick='myFunction()' className='savebot'>
            Salvar
          </button>{' '}
        </div>
      </div>
    </div>
  );
}

export default CursoDiretor;
