import React from 'react';
// import './CursoDiretorFormulario.css';
import Menu from '../../../common/Menu/Menu';
import Input from '../../../common/Input/Input';
import Button from '../../../common/Button/Button';
import { Context } from '../../../context/Context';

function CursoCoordenador() {
  return (
    // BASE
    <div className='base-screen'>
    <Menu />
    <div className='base-content'>
        {/* Começo do site */}
        <h1 id='titulo'>Curso</h1><br/><br/>
        <div className='linhatitulo'></div><br/><br/>
            <h3 className='TableSub'>Edite as informações do Curso</h3>
            <div> 
                <div className='wordsize'>
                    Nome do Curso:
                    <input type='text' className='barratamanho' />
                </div>
                <div className='wordsize'>
                    Área de Conhecimente:
                    <input type='text' className='barratamanho' />
                </div>
                <div className='wordsize'>
                    Campus/Sede:
                    <input type='text' className='barratamanho' />
                </div> <br/>
                <h3 className='TableSub'>Dados para Contato</h3><br/>
                <form action='' className='row2'>
                    <p>
                        <label for='' className='TableSubs'>
                        Telefone:
                        </label>
                        <input type='text' className='tamanhos' />
                    </p>
                    <p>
                        <label for='' className='TableSubs'>
                        Email:
                        </label>
                        <input type='text' className='tamanhos' />
                    </p>
                </form>
                <p><br/><br/><br/>
                <button onclick='myFunction()' class='buttoncara'>
                    Salvar
                </button>{' '}
                </p>
            </div>
      </div>
</div>
);
}

export default CursoCoordenador;