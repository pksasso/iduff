import React from 'react';
import './Cadastro-coordenador.css';
import Menu from '../../../common/Menu/Menu';
import { useLocation} from 'react-router-dom';
import { useHistory } from 'react-router-dom';

function CoordenadorCad() {
  const history = useHistory();
  const location = useLocation();
  const loggedData = JSON.parse(localStorage.getItem('userData'));

  const title = location.pathname.split('/')[2];

  return (
    // BASE
    <div className='base-screen'>
      <Menu />
      <div className='base-content'>
        {/* Começo do site */}
        <button
          onclick='myFunction()'
          className='boxexit'
          onClick={() => history.push('/listas-coordenador')}>X</button>{' '}
        <h1 id='titulo'>Coordenador</h1>
        <div className='caixatexto'>
          {/*TABELA NOME*/}
          <div className='displayhelp'>
            <form action=''>
              <table>
                <tbody>
                  {loggedData.role === 'manager' ? (
                    <tr>
                      <td className='TableSubs'>Tipo:</td>
                      <td>
                        <select name='exemplo1' className='tamanho' />
                      </td>
                    </tr>
                  ) : (
                    ''
                  )}
                  <br />
                  <tr>
                    <td className='TableSubs'>Nome:</td>
                    <td>
                      <input type='text' className='tamanho' />
                    </td>
                  </tr>
                </tbody>
              </table>
              <br />
            </form>
            <form action='' className='row'>
              <p>
                <label for='' className='TableSubs'>
                  RG
                </label>
                <input type='text' className='tamanhos' />
              </p>
              <p>
                <label for='' className='TableSubs'>
                  Estado
                </label>
                <select name='exemplo1' className='tamanhos' />
              </p>
              <p>
                <label for='' className='TableSubs'>
                  Nascimento
                </label>
                <input type='text' className='tamanhos' />
              </p>
              <p>
                <label for='' className='TableSubs'>
                  Matrícula
                </label>
                <input type='text' className='tamanhos' />
              </p>
              <p>
                <label for='' className='TableSubs'>
                  Senha:
                </label>
                <input type='text' className='tamanhos' />
              </p>
              <p>
                <label for='' className='TableSubs'>
                  CPF
                </label>
                <input type='text' className='tamanhos' />
              </p>
              </form>
                <p><br/><br/>
                  <button onclick='myFunction()' class='buttoncara'>
                    Salvar
                  </button>{' '}
                </p>
          </div>
        </div>
      </div>
    </div>
  );
}
export default CoordenadorCad;
