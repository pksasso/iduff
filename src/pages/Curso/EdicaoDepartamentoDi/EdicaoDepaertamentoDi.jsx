import React from 'react';
import './EdicaoDepartamentoDi.css';
import { useLocation } from 'react-router-dom';
import Menu from '../../../common/Menu/Menu';

function EdicaoDepartamentoDi() {
  const loggedData = JSON.parse(localStorage.getItem('userData'));
  const location = useLocation();
  const pageTilte = location.pathname.split('/')[2];
  return (
    // BASE
    <div className='base-screen'>
    <Menu />
    <div className='base-content'>
        {/* Começo do site */}
        <h1 id='titulo'>Departamento</h1><br/><br/>
        <div className='linhatitulo'></div><br/><br/>
            <h3 className='TableSub'>Edite as informações do Departamento</h3>
            <div> 
                <div className='wordsize'>
                    Nome do Departamento:
                    <input type='text' className='barratamanho' />
                </div>
                <div className='wordsize'>
                    Área de Conhecimente:
                    <input type='text' className='barratamanho' />
                </div>
                <div className='wordsize'>
                    Campus/Sede:
                    <input type='text' className='barratamanho' />
                </div>
                <div className='wordsize'>
                    Coordenador:
                    <select name='exemplo1' className='barratamanho' />
                </div><br/><br/>
                <h3 className='TableSub'>Dados para Contato</h3>
                <form action='' className='row2'>
                    <p>
                        <label for='' className='TableSubs'>
                        Telefone:
                        </label>
                        <input type='text' className='tamanhos' />
                    </p>
                    <p>
                        <label for='' className='TableSubs'>
                        Email:
                        </label>
                        <input type='text' className='tamanhos' />
                    </p>
                </form>
                <p><br/><br/>
                <button onclick='myFunction()' class='buttoncara'>
                    Salvar
                </button>{' '}
                </p>
            </div>
      </div>
</div>
);
}

export default EdicaoDepartamentoDi;
