import React, { useState } from 'react';
import Menu from '../../common/Menu/Menu';
import './EditProfile.css';

import Input from '../../common/Input/Input';
import Button from '../../common/Button/Button';

function EditProfile() {
  const [name, setName] = useState('');
  const [addres, setAddres] = useState('');
  const [homeNumber, setHomeNumber] = useState('');
  const [neighborhood, setNeighborhood] = useState('');
  const [complement, setComplement] = useState('');
  const [state, setState] = useState('');
  const [cep, setCep] = useState('');
  const [tel, setTel] = useState();
  const [cell, setCell] = useState('');

  const handleSumbit = () => {
    console.log(name);
    console.log(addres);
    console.log(homeNumber);
    console.log(neighborhood);
    console.log(complement);
    console.log(state);
    console.log(cep);
    console.log(tel);
    console.log(cell);
  };

  return (
    <div className='base-screen'>
      <Menu />
      <div className='base-content'>
        <div className='edit-profile-wrapper'>
          <h1 className='edit-profile-title'>Editar perfil</h1>
          <p>Edite os dados cadastrados para sua conta IDUFF</p>
          <p>{`Nacionalidade: Brasileiro`}</p>
          <p>{`Estado: Rio de janeiro`}</p>
          <div className='inline-inputs'>
            <p>{`RG: 12.345.678-9`}</p>
            <p>{`CPF: 123.456.789-12`}</p>
          </div>
          <p>Dados para contato</p>
          <Input
            labelText='Nome completo'
            labelPosition='label-left'
            inputType='text'
            state={name}
            setState={setName}
          />
          <div className='container-line'>
            <div className='edit-input-wrapper-3'>
              <Input
                labelText='Rua:'
                labelPosition='label-top'
                inputType='text'
                state={addres}
                setState={setAddres}
              />
            </div>
            <div className='edit-input-wrapper-2'>
              <Input
                labelText='Nº: '
                labelPosition='label-top'
                inputType='text'
                state={homeNumber}
                setState={setHomeNumber}
              />
            </div>
            <div className='edit-input-wrapper-2'>
              <Input
                labelText='Bairro: '
                labelPosition='label-top'
                inputType='text'
                state={neighborhood}
                setState={setNeighborhood}
              />
            </div>
          </div>
          <div className='container-line'>
            <div className='edit-input-wrapper-1'>
              <Input
                labelText='Complemento:'
                labelPosition='label-top'
                inputType='text'
                state={complement}
                setState={setComplement}
              />
            </div>
            <div className='edit-input-wrapper-1'>
              <Input
                labelText='Estado: '
                labelPosition='label-top'
                inputType='text'
                state={state}
                setState={setState}
              />
            </div>
            <div className='edit-input-wrapper-1'>
              <Input
                labelText='CEP: '
                labelPosition='label-top'
                inputType='text'
                state={cep}
                setState={setCep}
              />
            </div>
          </div>
          <div className='container-line two-itens'>
            <div className='edit-input-wrapper-1 last-line'>
              <Input
                labelText='Telefone: '
                labelPosition='label-top'
                inputType='text'
                state={tel}
                setState={setTel}
              />
            </div>
            <div className='edit-input-wrapper-1 last-line'>
              <Input
                labelText='Celular: '
                labelPosition='label-top'
                inputType='text'
                state={cell}
                setState={setCell}
              />
            </div>
          </div>
          <div className='edit-button-wrapper'>
            <Button
              handleClick={handleSumbit}
              text='Salvar'
              buttonLocation='edit-button-wrapper'
            />
          </div>
        </div>
      </div>
    </div>
  );
}

export default EditProfile;
