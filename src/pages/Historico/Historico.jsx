import React from 'react';
import './Historico.css';
import Menu from '../../common/Menu/Menu';
import Table from '../../common/Table/Table';

function Historico() {
  const teste = [
    {
      id: 1,
      name: 'Calculo I',
      status: 'Reprovado',
      nota: '1.0',
    },
    {
      id: 2,
      name: 'Calculo I',
      status: 'Reprovado',
      nota: '2.0',
    },
    {
      id: 3,
      name: 'Calculo I',
      status: 'Reprovado',
      nota: '3.0',
    },
    {
      id: 4,
      name: 'Calculo I',
      status: 'Reprovado',
      nota: '4.0',
    },
    {
      id: 5,
      name: 'Calculo I',
      status: 'Reprovado',
      nota: '5.0',
    },
    {
      id: 6,
      name: 'Calculo I',
      status: 'Reprovado',
      nota: '5.5',
    },
    {
      id: 7,
      name: 'Calculo I',
      status: 'Aprovado',
      nota: '6.0',
    },
    {
      id: 8,
      name: 'Calculo I',
      status: 'Aprovado',
      nota: '7.0',
    },
    {
      id: 9,
      name: 'Calculo I',
      status: 'Aprovado',
      nota: '8.0',
    },
    {
      id: 10,
      name: 'Calculo I',
      status: 'Aprovado',
      nota: '9.0',
    },
  ];

  return (
    <div className='base-screen'>
      <Menu />
      <div className='base-content'>
        <h1 id='titulo'>Histórico</h1>
        <Table titles={['Nome', 'Status', 'Nota']} items={teste} />
      </div>
    </div>
  );
}

export default Historico;
