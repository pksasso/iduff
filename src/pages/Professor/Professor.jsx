import React from 'react';
import './Professor.css';
import Menu from '../../common/Menu/Menu';
import { useHistory } from 'react-router-dom';

const teste = [
  { name: 'Laboratorio', period: '7° periodo', id: '50', vagas: '13/40' },
  { name: 'Laboratorio', period: '7° periodo', id: '50', vagas: '13/40' },
  { name: 'Laboratorio', period: '7° periodo', id: '50', vagas: '13/40' },
  { name: 'Laboratorio', period: '7° periodo', id: '50', vagas: '13/40' },
  { name: 'Laboratorio', period: '7° periodo', id: '50', vagas: '13/40' },
];

function Professor() {
  const history = useHistory();
  return (
    <div className='base-screen'>
      <Menu />
      <div className='base-content'>
        <button onclick='myFunction()' className='boxexit' onClick={() => history.push('/listas-professor')}>X </button>{' '}
        <h1 id='titulo'>Professores</h1>
        <div className='Professores'>
          <h3 className='TableSub'>"Departamento:GMA"</h3>
          <br/><form action='' className='gridprof'>
            <div className='label1prof'>
              <label for=''>Nome:</label>
            </div>
            <div className='input1prof'>
              <input type='text' id='input1prof' />
            </div>
            <div className='label2prof'>
              <label for=''>RG:</label>
            </div>
            <div className='input2prof'>
              <input type='text' id='input2prof' />
            </div>
            <div className='label3prof'>
              <label for=''>Estado</label>
            </div>
            <div className='input3prof'>
              <input type='text' id='input3prof' />
            </div>
            <div className='label4prof'>
              <label for=''>Nascimento:</label>
            </div>
            <div className='input4prof'>
              <input type='text' id='input4prof' />
            </div>
            <div className='label5prof'>
              <label for=''>Matricula:</label>
            </div>
            <div className='input5prof'>
              <input type='text' id='input5prof' />
            </div>
            <div className='label6prof'>
              <label for=''>Senha:</label>
            </div>
            <div className='input6prof'>
              <input type='text' id='input6prof' />
            </div>
            <div className='label7prof'>
              <label for=''>CPF</label>
            </div>
            <div className='input7prof'>
              <input type='text' id='input7prof' />
            </div>
            <div className='label8prof'>
              <label for=''>Adicionar materia para o professor</label>
            </div>
            <div className='select1prof'>
              <select id='select1prof'></select>
            </div>
            <div className='button1prof'>
              <button onClick='function()' id='button1prof'>Adicionar</button><br/><br/>
            </div>
          </form>
        </div>
        <div className="Cardsprof">
          {teste.map(u => (
            <div className="eachCard">
              <div className="nomeprof"><label>{u.name}</label></div>
              <div className="periodoprof"><label>{u.period}</label></div>
              <div className="buttonprof"><button onClick="Myfunction({u.id})" id="deleteprof">Excluir</button></div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}

export default Professor;
