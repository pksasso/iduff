import React from 'react';
import './AddMaterias.css';
import Menu from '../../common/Menu/Menu';
import { useHistory } from 'react-router-dom';
import Button from '../../common/Button/Button';

const teste = [
  {
    id: 1,
    name: 'Calculo I',
    knowledge_area: 'Matematica',
    quantity: '60',
  },
  {
    id: 2,
    name: 'Prog II',
    knowledge_area: 'Prog I',
    quantity: '60',
  },
  {
    id: 3,
    name: 'PROBEST',
    knowledge_area: 'Calculo',
    quantity: '70',
  }
];

function AddMaterias() {
  const history = useHistory();
  return (
    <div className='base-screen'>
    <Menu />
    <div className='base-content'>
      <h1 id='titulo'>Adcionar Matérias</h1><br/><br/>
      <div className = 'linhatitulo'></div>
      <div className='schoolyear-head'>
        <div className='schoolyear-data'>
        </div>
      </div>
        <div className='wordsize2'>
            Nome:
            <input type='text' className='barratamanho' />
        </div>
        <form action='' className='size'>
          <p>
              <label for='' className='TableSubs'>
              Área de Conhecimento
              </label>
              <input type='text' className='tamanhos' />
          </p>
          <p>
              <label for='' className='TableSubs'>
              CHT
              </label>
              <input type='text' className='tamanhos' />
          </p>
          <p>
              <label for='' className='TableSubs'>
              Adcionar Pré-requisitos:
              </label>
              <input type='text' className='tamanhos' />
          </p>
          <p>
            <button onclick='myFunction()' class='button'>
              Adcionar
            </button>{' '}
          </p>
      </form>
        <div className='TableSubs'>Pré-Requisitos:</div>
      {/* API AQUI ----------------------------------------------- */}
      <div className="meio">
        {teste.map(u => (
          <div className = 'flores'>
            <div>{u.name}</div>
            <div>{u.knowledge_area}</div>
            <div><button
                    onclick='myFunction()'
                    className="boxexitadd"
                    onClick={() => history.push('/addmaterias')}>Excluir</button>{' '}
            </div>
          </div>
          
        ))}
    </div>
      {/* //API!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */}
    </div>
  </div> 
  );
}
export default AddMaterias;