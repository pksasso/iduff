// import React, { Component } from 'react';
// import './App.css';
// import 'font-awesome/css/font-awesome.css';
// import { InputText } from 'primereact/components/inputtext/InputText';
// import axios from 'axios';
// import './bootstrap/css/bootstrap.css';
// import logo from './logo.png';
// import Server from './Server';


// class App extends Component {
// constructor() {
//     super();
//     this.state = {
//         Produtos: [],
//         Produto: '',
//         Mensagem: ''

//     }

// }

// componentWillMount() {
//     axios.get(Server + `/produto/listar`)
//         .then(
//             (result) => {
//                 this.setState(result.data);
//                 if (this.state.Produtos.length > 0) {
//                     this.setState({ Produto: this.state.Produtos[0] });
//                 }
//             })
// }

// novo = () => {
//     this.setState({ Produto: { "id": 0, "descricao": "Novo" } });
// }

// gravar = () => {
//     if (this.state.Produto.descricao.length > 100) {
//         this.setState({ Mensagem: 'Máximo de 100 posições.' });
//         return;
//     }
//     if (this.state.Produto.id === 0) {
//         this.inserir();
//     } else {
//         this.atualizar();
//     }
//     this.setState({ Mensagem: 's.' });
// }

// inserir = () => {
//     const d = this.state.Produto.descricao;
//     axios.get(Server + `/produto/inserir?descricao=${d}`)
//         .then(
//             (result) => {
//                 this.setState({ Produto: result.data });
//                 this.setState({ Produtos: [...this.state.Produtos, this.state.Produto] });
//             })
// }

// atualizar = () => {
//     const i = this.state.Produto.id;
//     const d = this.state.Produto.descricao;
//     axios.get(Server + `/produto/atualizar?id=${i}&descricao=${d}`)
//         .then(
//             (result) => {
//                 var i = this.state.Produtos.findIndex(x => x.id === this.state.Produto.id);
//                 this.setState({ Produto: result.data });
//                 var Produtos = [...this.state.Produtos];
//                 Produtos[i] = this.state.Produto;
//                 this.setState({ Produtos });
//             });
// }

// apagar = () => {
//     if (this.state.Produto.id > 0) {
//         const i = 186;
//         axios.get(Server +`/produto/apagar?id=${i}`)
//             .then(
//                 (result) => {
//                     var i = this.state.Produtos.findIndex(x => x.id === this.state.Produto.id);
//                     var Produtos = [...this.state.Produtos];
//                     Produtos.splice(i, 1);
//                     this.setState({ Produtos });
//                     if (this.state.Produtos.length > 0) {
//                         this.setState({ Produto: Produtos[0] });
//                     }
//                 });
//     }
// }

// render() {
//     const { Produtos, Produto, Mensagem } = this.state

//     return (

//         <div className={"App"}>
//             <span>{Mensagem}</span>

//             <div className={"container"}>
//                 <div className={"row"}>
//                     <div className={"col-md-1 logo"}>
//                         <img className={"img-logo"} src={logo} alt="logo" />
//                     </div>
//                     <div className={"col-4"}>
//                         <label>ID:{Produto.id}</label>
//                     </div>
//                     <div className={"col-4"}>
//                         <InputText value={Produto.descricao} onChange={(e) => this.setState({ Produto: { id: Produto.id, descricao: e.target.value } })} />
//                         <button type="button" onClick={this.gravar} className="btn btn-primary">Gravar</button>
//                     </div>
//                 </div>

//                 <div className={"row justify-content-center"}>
//                     <div clasName="row">
//                         <div className={"col-3"}>
//                             <button type="button" onClick={this.novo} className="btn btn-primary">Novo</button>
//                         </div>
//                     </div>
//                     <div className={"row"}>
//                         <div className={"col-3"}>
//                             <button type="button" onClick={this.apagar} className="btn btn-danger">Apagar</button>
//                         </div>
//                     </div>
//                     <div className={"row"}>
//                         <div className={"col-3"}>
//                             <button type="button" onClick={this.apagar} className="btn btn-info">Alterar</button>
//                         </div>
//                     </div>
//                 </div>
//             </div>

//             <table id="my_table" className="table table-hover table-responsive-md">
//                 <thead>
//                     <tr>
//                         <th>Id</th>
//                         <th>Descricao</th>
//                         <th>Index</th>
//                         <th>Ações</th>

//                     </tr>
//                 </thead>
//                 <tbody>
//                     {
//                         Produtos.map((Produto, index) => { 

//                             return (
//                                 <tr key={index}>
//                                     <td>{Produto.id}</td>
//                                     <td>{Produto.descricao}</td>
//                                     <td>{index}</td>
//                                     <td>
//                                         <button className="btn btn-danger" onClick={this.apagar}>Deletar</button>
//                                         <button className="btn btn-render btn-warning " >Alterar</button>
//                                     </td>
//                                 </tr>
//                             )
//                         })
//                     }
//                 </tbody>
//             </table>

//         </div>

//     )
// }
// }

// export default App;