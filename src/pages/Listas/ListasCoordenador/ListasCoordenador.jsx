import React, { useEffect, useState, useContext } from 'react';
import './ListasCoordenador.css';
import Menu from '../../../common/Menu/Menu';
import { useHistory } from 'react-router-dom';
import Table from '../../../common/Table/Table';

const teste = [
    {
      id: 'Carlos',
      cpf: '156.987.987-76',
      rg: '687063802',
      depart: 'GMA',
    },
    {
      id: 'Carla',
      cpf: '180.987.987-76',
      rg: '987063802',
      depart: 'GMA',
    },
    {
      id: 'Tais',
      cpf: '089.987.987-76',
      rg: '687063802',
      depart: 'GMA',
    },
    {
      id: 'Bruno',
      cpf: '109.987.987-76',
      rg: '876063802',
      depart: 'GMA',
    },
    
   
  ];


function Lista () {
    const history = useHistory();
    return (
        // BASE
        <div className='base-screen'>
            <Menu />
            <div className='base-content'>
                {/* Começo do site */}
                <button type="submit" className='boxadd' onClick={() => history.push('/cadastro-coordenador')} >+</button>
                <h1 id='titulo'>Coordenadores</h1><br/>
                <div className = 'linhatitulo'></div>
                <div className='caixatexto'>
                    {/*TABELA NOME*/}
                    <div className = 'subflor3'>
                    <div>Nome</div>
                    <div>CPF</div>
                    <div>RG</div>
                    <div>Depart/Curso</div>
                  </div>
                    {/* API AQUI ----------------------------------------------- */}
                    <div className="meio">
                      {teste.map(u => (
                        <div className = 'tabelaflor3'>
                          <div>{u.id}</div>
                          <div>{u.cpf}</div>
                          <div>{u.rg}</div>
                          <div>{u.depart}</div>
                          </div>
                      ))}
                  </div>
                  {/* //API!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */}
                </div>
            </div>
        </div>
);
}
export default Lista