import React, { useEffect, useState, useContext } from 'react';
import './ListasProfessor.css';
import Menu from '../../../common/Menu/Menu';
import { useHistory } from 'react-router-dom';
import Table from '../../../common/Table/Table';

const teste = [
    {
      id: 'Carla',
      name: 'Calculo I',
      status: '109.987.987-76',
      nota: '876063802',
    },
    {
      id: 'Carla',
      name: 'Calculo I',
      status: '109.987.987-76',
      nota: '876063802',
    },
    {
      id: 'Carla',
      name: 'Calculo I',
      status: '109.987.987-76',
      nota: '876063802',
    },
    {
      id: 'Carla',
      name: 'Calculo I',
      status: '109.987.987-76',
      nota: '876063802',
    },
    {
      id: 'Carla',
      name: 'Calculo I',
      status: '109.987.987-76',
      nota: '876063802',
    },
    {
      id:'Carla',
      name: 'Calculo I',
      status: '109.987.987-76',
      nota: '876063802',
    },
    {
      id:'Carla',
      name: 'Calculo I',
      status: '109.987.987-76',
      nota: '876063802',
    },
    {
      id: 'Carla',
      name: 'Calculo I',
      status: '109.987.987-76',
      nota: '876063802',
    },
  ];


function ListaAluno () {
    const history = useHistory();
    return (
        // BASE
        <div className='base-screen'>
            <Menu />
            <div className='base-content'>
                {/* Começo do site */}
                <button type="submit" className='boxadd' onClick={() => history.push('/professor')} >+</button>
                <h1 id='titulo'>Professores</h1><br/>
                <div className = 'flor2'>
                <div>Nome</div>
                <div>CPF</div>
                <div>RG</div>
                <div>Turmas</div>
              </div>
                <div className="Cardsprof">
                {teste.map(u => (
                  <div className = 'flor'>
                    <div>{u.id}</div>
                    <div>{u.status}</div>
                    <div>{u.nota}</div>
                    <div> {u.name} </div>
                  </div>
                  
                ))}
            </div>
            </div>
        </div>
);
}
export default ListaAluno