import React, { useEffect, useState, useContext } from 'react';
import './ListasAluno.css';
import Menu from '../../../common/Menu/Menu';
import { useHistory } from 'react-router-dom';
import Table from '../../../common/Table/Table';

const teste = [
  {
    id: 'Carlos',
    cpf: '156.987.987-76',
    rg: '687063802',
    depart: '1º Período',
  },
  {
    id: 'Carla',
    cpf: '180.987.987-76',
    rg: '987063802',
    depart: '2º Período',
  },
  {
    id: 'Tais',
    cpf: '089.987.987-76',
    rg: '687063802',
    depart: '3º Período',
  },
  {
    id: 'Bruno',
    cpf: '109.987.987-76',
    rg: '876063802',
    depart: '1º Período',
  },
  {
    id: 'Tais',
    cpf: '089.987.987-76',
    rg: '687063802',
    depart: '3º Período',
  },
  {
    id: 'Bruno',
    cpf: '109.987.987-76',
    rg: '876063802',
    depart: '1º Período',
  },
  
 
];

function ListaAluno () {
    const history = useHistory();
    return (
        // BASE
        <div className='base-screen'>
            <Menu />
            <div className='base-content'>
                {/* Começo do site */}
                <button type="submit" className='boxadd' onClick={() => history.push('/cadastro-aluno')} >+</button>
                <h1 id='titulo'>Alunos</h1><br/>
                <div className = 'linhatitulo'></div>
                <div className='caixatexto'>
                    {/*TABELA NOME*/}
                    <div className = 'subflor3'>
                    <div>Nome</div>
                    <div>RG</div>
                    <div>CPF</div>
                    <div>Periodo</div>
                  </div>
                    {/* API AQUI ----------------------------------------------- */}
                    <div className="meio">
                      {teste.map(u => (
                        <div className = 'tabelaflor3'>
                          <div>{u.id}</div>
                          <div>{u.rg}</div>
                          <div>{u.cpf}</div>
                          <div>{u.depart}</div>
                          </div>
                      ))}
                  </div>
                </div>
            </div>
        </div>
);
}
export default ListaAluno