import React from 'react';
import './Inscricao.css';
import Menu from '../../common/Menu/Menu';
import Table from '../../common/Table/Table';
import { useHistory } from 'react-router-dom';
// TESTE!!!!!!!!!!!
const teste = [
  {
    name: 'Calculo I',
    period: '2° periodo',
    cht: '60',
    vagas: '15/50',
  },
  { name: 'Laboratorio', period: '7° periodo', cht: '50', vagas: '13/40' },
  { name: 'Geometria', period: '8° periodo', cht: '35', vagas: '2/40' },
  { name: 'Prog I', period: '1° periodo', cht: '60', vagas: '13/60' },
  { name: 'PROBEST', period: '3° periodo', cht: '70', vagas: '3/10' },
  { name: 'FMC', period: '1° periodo', cht: '60', vagas: '13/40' },
  { name: 'SO', period: '3° periodo', cht: '60', vagas: '13/40' },
  { name: 'Empreendedorismo', period: '5° periodo', cht: '45', vagas: '13/40' },
  { name: 'Calculo II', period: '7° periodo', cht: '80', vagas: '13/40' },
];

function Inscricao() {
  const history = useHistory();
  return (
    <div className='base-screen'>
      <Menu />
      <div className='base-content'>
        <h1 id='titulo'>Inscrição em Matérias</h1><br/><div className = 'linhatitulo'></div><br/><br/>
        <div className = 'subflor3'>
            <div>Nome</div>
            <div>Vagas</div>
            <div>Período</div>
            <div>CHT</div>
          </div>
          {/* API AQUI ----------------------------------------------- */}
            <div className="meio">
              {teste.map(u => (
                <div className = 'tabelaflor3'>
                  <div>{u.name}</div>
                  <div>{u.period}</div>
                  <div>{u.cht}</div>
                  <div>{u.vagas}</div>
                  <div><button
                    onclick='myFunction()'
                    className="boxmanager"
                    onClick={() => history.push('/inscricao')}>Inscrever</button>{' '}</div>
                  </div>
              ))}
          </div>
      {/* //API!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */}
      </div>
    </div>
  );
}
export default Inscricao;
